from setuptools import setup, find_packages

import ytreza

setup(
    name='ytreza',

    version=ytreza.__version__,
    packages=find_packages(),
    author="Johjo",
    author_email="johjo@ytreza.org",
    description="Ytreza lib tool",
    long_description=open('README.md').read(),
    include_package_data=True,
    url='www.ytreza.org',

    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.5",
        "Topic :: Software Development",
    ],
)
