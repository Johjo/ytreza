from abc import ABCMeta


class BaseDomainModel(metaclass=ABCMeta):
    pass


class DomainModel(BaseDomainModel):
    classes = {}

    def __init__(self, *args, **kwargs):
        for field_name in dir(self):
            field = getattr(self, field_name, None)

            if field is not None and isinstance(field, Field):
                value = kwargs.get(field_name, None)
                if isinstance(value, dict):
                    if "__class__" in value:
                        if value["__class__"] in DomainModel.classes:
                            value = DomainModel.classes[
                                value["__class__"]](**value)

                setattr(self, field_name, value)

    @classmethod
    def register(cls, cls_registered):
        BaseDomainModel.register(cls_registered)
        cls.classes[cls_registered.__name__] = cls_registered

    @classmethod
    def from_dict(cls, adict):
        o = cls(**adict)
        return o

    def to_dict(self):
        d = {}
        for key, value in self.__dict__.items():
            if value is not None:
                if isinstance(value, DomainModel):
                    d[key] = value.to_dict()
                else:
                    d[key] = value
        d["__class__"] = self.__class__.__name__
        return d

    def clone(self, **kwargs):
        d = self.to_dict()
        d.update(kwargs)
        return self.__class__().from_dict(d)

    def __eq__(self, o):
        try:
            return o.to_dict() == self.to_dict()
        except AttributeError:
            return False


class Field:
    pass
