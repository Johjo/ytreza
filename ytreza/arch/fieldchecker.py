def text_is_empty(text):
    return text is None or text.strip() == ""


def latitude_is_valid(geo):
    if geo is None:
        return False
    elif geo.latitude is None:
        return False
    else:
        return -90.0 <= geo.latitude <= 90


def longitude_is_valid(geo):
    if geo is None:
        return False
    elif geo.longitude is None:
        return False
    else:
        return -180.0 <= geo.longitude <= 180.0
