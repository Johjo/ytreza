from abc import ABCMeta, abstractmethod
from ytreza.arch.response_object import ResponseFailure as RF


class UseCase(metaclass=ABCMeta):

    def execute(self, request_object):
        if not request_object:
            return RF.build_from_invalid_request_object(request_object)
        try:
            response = self.process_request(request_object)
            return response
        except Exception as exc:
            return RF.build_from_exception(exc)

    @abstractmethod
    def process_request(self, request_object):
        pass


class UseCaseAsynchronous(metaclass=ABCMeta):

    async def execute(self, request_object):
        if not request_object:
            return RF.build_from_invalid_request_object(request_object)
        try:
            response = await self.process_request(request_object)
            return response
        except Exception as exc:
            return RF.build_from_exception(exc)

    @abstractmethod
    async def process_request(self, request_object):
        pass
