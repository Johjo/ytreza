from collections import OrderedDict


class InvalidRequestObject:
    def __init__(self):
        self.errors = []

    def add_error(self, parameter, message):
        self.errors.append({'parameter': parameter, 'message': message})

    def has_errors(self):
        return len(self.errors) > 0

    def __nonzero__(self):
        return False

    def __repr__(self):
        seq = [(error['parameter'], error['message']) for error in self.errors]
        return "\n".join(["{} : {}".format(parameter, message)
                          for parameter, message in seq])

    __bool__ = __nonzero__


def extract_value(d, key):
    path = key.split(".")
    if len(path) == 1:
        return d.get(key, None)
    else:
        return extract_value(d.get(path[0], None).__dict__, ".".join(path[1:]))


class OrderedMeta(type):
    @classmethod
    def __prepare__(metacls, name, bases):
        return OrderedDict()

    def __new__(cls, name, bases, clsdict):
        c = type.__new__(cls, name, bases, clsdict)
        c._orderedKeys = clsdict.keys()
        return c


class ValidRequestObject(metaclass=OrderedMeta):
    def __new__(cls, **kwargs):
        invalid_req = InvalidRequestObject()

        parameters = []
        validators = []
        values = {}

        for attr_name in cls._orderedKeys:
            attr = getattr(cls, attr_name)

            if isinstance(attr, Parameter):
                parameters.append(attr)
            elif isinstance(attr, Validator):
                validators.append(attr)

        for p in parameters:
            value = extract_value(kwargs, p.path)
            if value is None:
                value = p.default

            if not p.null and value is None:
                invalid_req.add_error(p.path, "Is mandatory")
            elif not p.empty and not value:
                invalid_req.add_error(p.path, "Can't be empty")
            else:
                values[p.path] = value

        if not invalid_req.has_errors():
            for v in validators:
                value = extract_value(kwargs, v.path)
                if not v.null and value is None:
                    invalid_req.add_error(v.path, "Is mandatory")
                elif not v.empty and not value:
                    invalid_req.add_error(v.path, "Can't be empty")
                else:
                    values[v.path] = value

        if invalid_req.has_errors():
            return invalid_req

        inst = super(ValidRequestObject, cls).__new__(cls)

        for (parameter, message) in cls.validate(**values):
            invalid_req.add_error(parameter, message)

        if invalid_req.has_errors():
            return invalid_req

        return inst

    def __init__(self, **kwargs):
        parameters = []
        validators = []
        values = {}
        cls = self.__class__

        for attr_name in vars(cls):
            attr = getattr(cls, attr_name)

            if isinstance(attr, Parameter):
                parameters.append((attr_name, attr))
            elif isinstance(attr, Validator):
                validators.append((attr_name, attr))

        for name, p in parameters:
            value = extract_value(kwargs, p.path)
            if value is None:
                value = p.default

            if (p.null or value is not None) and (p.empty or value):
                values[name] = value

        for name, v in validators:
            value = extract_value(kwargs, v.path)
            if not v.null and value is None:
                pass
            elif not v.empty and not value:
                pass
            else:
                values[name] = value

        self.__dict__.update(values)

    @classmethod
    def from_dict(cls, adict):
        raise NotImplementedError

    def has_errors(self):
        return False

    def __nonzero__(self):
        return True

    @classmethod
    def validate(cls, **kwargs):
        yield from []

    __bool__ = __nonzero__


class Parameter:
    def __init__(self, path, empty=False, null=False, default=None):
        self.path = path
        self.empty = empty
        self.null = null
        self.default = default


class Validator:
    def __init__(self, path, empty=False, null=False):
        self.path = path
        self.empty = empty
        self.null = null
