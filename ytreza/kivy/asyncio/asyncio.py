import asyncio
from kivy.clock import Clock


__all__ = ["AsyncioBuiltIn"]


class AsyncioBuiltIn:
    """This class provide an access to asyncio with kivy"""

    def __init__(self, *args, **kwargs):
        super().__init_(*args, **kwargs)
        self._started = False

    @property
    def loop(self):
        """this property return the asyncio eventloop

        Returns:
            EventLoop: eventloop of asyncio
        """
        if not hasattr(self, "_loop"):
            self._loop = asyncio.get_event_loop()
        return self._loop

    def to_asyncio(self, func, *args, **kwargs):
        """put a function in the asyncio eventloop

        Args:
            func (function): function to run in asyncio eventloop
            *args, **kwargs : arguments of the function

        Returns:
            Asyncio.Task: task which runs in asyncio
        """
        return self.loop.create_task(func, *args, **kwargs)

    def gather_to_asyncio(self, *coros_or_futures):
        """gather in asyncio a list of coroutines or futures

        Args:
            *coros_or_futures : coroutines or futures to gather

        Returns:
            TYPE: result of gathering futures
        """
        return self.loop.gather(*coros_or_futures)

    def wait_for_asyncio(self, func, *args, **kwargs):
        """execute a function in asyncio and wait for the result.

        Args:
            func (function): function to execute
            *args
            **kwargs

        Returns:
            TYPE: result of the function
        """
        return self.loop.run_until_complete(func, *args, **kwargs)

    def on_asyncio_tick(self):
        """execute a tick of asyncio. This function shoud be call every time
        by kivy with clock functions.

        Returns:
            TYPE: None
        """
        self.loop.stop()
        self.loop.run_forever()

    def start(self, tick_delay=0, loop=None):
        """start the loop

        Args:
            tick_delay (int, optional): delay between two ticks
            loop (None, optional): asyncio loop

        Returns:
            TYPE: Description
        """
        super().start()
        if not self._started:
            if loop:
                self._loop = loop
            Clock.schedule_interval(self.on_asyncio_tick, tick_delay)


class Controller:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def start(self):
        super().start()
        pass
