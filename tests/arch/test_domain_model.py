from ytreza.arch.domain_model import DomainModel
from ytreza.arch import domain_model as dm


class ModelTest(DomainModel):
    value_1 = dm.Field()
    value_2 = dm.Field()


class ModelTest2(DomainModel):
    value_3 = dm.Field()
    value_4 = dm.Field()


DomainModel.register(ModelTest)
DomainModel.register(ModelTest2)


def test_to_dict():
    t = ModelTest()
    assert t.to_dict() == {'__class__': "ModelTest"}

    t = ModelTest(value_1=5)
    assert t.to_dict() == {'__class__': "ModelTest", 'value_1': 5}

    t = ModelTest(value_2=3)
    assert t.to_dict() == {'__class__': "ModelTest", 'value_2': 3}

    t = ModelTest(value_1=4, value_2=6)
    assert t.to_dict() == {'__class__': "ModelTest",
                           'value_1': 4,
                           'value_2': 6}


def test_fields_without_inheritance():
    t1 = ModelTest(value_1="toto", value_2=[2, 3, 6, "papa"])

    assert isinstance(t1.value_1, str)
    assert isinstance(t1.value_2, list)


def test_fields_with_inheritance():
    class ModelTest2(ModelTest):
        def __init__(self, value_1=None, value_2=None):
            super().__init__(value_1=[1, 2, 3], value_2="value 1")

    t2 = ModelTest2()
    assert isinstance(t2.value_1, list)
    assert isinstance(t2.value_2, str)


def test_from_dict():
    t1 = ModelTest(value_1="toto", value_2=[2, 3, 6, "papa"])
    t2 = ModelTest.from_dict(t1.to_dict())

    assert t1.value_1 == t2.value_1
    assert t1.value_2 == t2.value_2


def test_clone():
    t1 = ModelTest(value_1="toto", value_2=[2, 3, 6, "papa"])
    t2 = t1.clone()

    assert t2.value_1 == t1.value_1
    assert t2.value_2 == t1.value_2

    t2 = t1.clone(value_1="titi")
    assert t2.value_1 == "titi"
    assert t2.value_2 == t1.value_2

    t2 = t1.clone(value_1="titi", value_2=[4, 5])
    assert t2.value_1 == "titi"
    assert t2.value_2 == [4, 5]


def test_clone_with_null():
    t1 = ModelTest(value_1="toto", value_2=[2, 3, 6, "papa"])
    t2 = t1.clone(value_1=None)

    assert t2.value_1 is None
    assert t2.value_2 is t1.value_2


def test_equal():
    t1 = ModelTest(value_1="toto", value_2=[2, 3, 6, "papa"])

    assert t1.clone() == t1
    assert not t1.clone(value_1="papa") == t1

    t1 = ModelTest(value_1=ModelTest2(value_3="3", value_4="4"), value_2="2")
    t2 = ModelTest(value_1=ModelTest2(value_3="3", value_4="4"), value_2="2")

    assert t1 == t2


def test_not_equal():
    t1 = ModelTest(value_1=ModelTest2(value_3="3", value_4="4"), value_2="2")
    assert not t1 == "text"
    assert not t1 == ["list"]
    assert not t1 == ModelTest(value_1=ModelTest2(
        value_3="3 bis", value_4="4"), value_2="2")


def test_to_dict_with_sub_model():
    t1 = ModelTest(value_1=ModelTest2(value_3="3", value_4="4"), value_2="2")

    print(t1.to_dict())

    assert t1.to_dict() == {
        '__class__': 'ModelTest',
        'value_1': {
            '__class__': 'ModelTest2',
            'value_3': "3",
            'value_4': "4"
        },
        'value_2': "2"
    }


def test_from_dict_with_sub_model():
    t1 = ModelTest(value_1=ModelTest2(value_3="3", value_4="4"), value_2="2")
    t2 = ModelTest.from_dict(t1.to_dict())

    assert t1.value_1 == t2.value_1
    assert t1.value_2 == t2.value_2
    assert t1.value_1.value_3 == t2.value_1.value_3
    assert t1.value_1.value_4 == t2.value_1.value_4
    assert t1.to_dict() == t2.to_dict()
    assert t1 == t2
