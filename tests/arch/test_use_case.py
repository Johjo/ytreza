import pytest
import asyncio
from unittest import mock
from ytreza.arch import use_case as uc
from ytreza.arch import response_object as rep
from ytreza.arch import request_object as req


class SomeUseCase(uc.UseCase):
    def process_request(self, request_object):
        return rep.ResponseSuccess("success")


uc.UseCase.register(SomeUseCase)


def test_use_case_can_process_request():
    request_object = req.ValidRequestObject()

    use_case = SomeUseCase()
    response = use_case.execute(request_object)

    print(response.value)

    assert bool(response)
    assert response.value == "success"


def test_use_case_can_process_invalid_request_and_return_failure():
    invalid_request_object = req.InvalidRequestObject()
    invalid_request_object.add_error("someparam", "somemessage")

    use_case = SomeUseCase()
    response = use_case.execute(invalid_request_object)

    assert not response
    assert response.type == rep.ResponseFailure.PARAMETERS_ERROR
    assert response.message == "someparam: somemessage"


def test_use_case_can_manage_generic_exception_from_process_request():
    use_case = SomeUseCase()

    class TestException(Exception):
        pass

    use_case.process_request = mock.Mock()
    use_case.process_request.side_effect = TestException('some message')

    response = use_case.execute(mock.Mock)

    assert not response
    assert response.type == rep.ResponseFailure.SYSTEM_ERROR
    assert response.message == "TestException: some message"


class SomeUseCaseAsynchronous(uc.UseCaseAsynchronous):
    async def process_request(self, request_object):
        await asyncio.sleep(0)
        return rep.ResponseSuccess("success")


uc.UseCaseAsynchronous.register(SomeUseCaseAsynchronous)


@pytest.mark.asyncio
async def test_use_case_asynchronous_can_process_asynchronous_request():
    request_object = req.ValidRequestObject()

    use_case = SomeUseCaseAsynchronous()
    response = await use_case.execute(request_object)
    print(response)

    assert bool(response)
    assert response.value == "success"


@pytest.mark.asyncio
async def test_use_case_asynchronous_can_process_invalid_request():
    invalid_request_object = req.InvalidRequestObject()
    invalid_request_object.add_error("someparam", "somemessage")

    use_case = SomeUseCaseAsynchronous()
    response = await use_case.execute(invalid_request_object)

    assert not response
    assert response.type == rep.ResponseFailure.PARAMETERS_ERROR
    assert response.message == "someparam: somemessage"


@pytest.mark.asyncio
async def test_use_case_asynchronous_can_manage_generic_exception():
    use_case = SomeUseCaseAsynchronous()

    class TestException(Exception):
        pass

    use_case.process_request = mock.Mock()
    use_case.process_request.side_effect = TestException('some message')

    response = await use_case.execute(mock.Mock)

    assert not response
    assert response.type == rep.ResponseFailure.SYSTEM_ERROR
    assert response.message == "TestException: some message"
