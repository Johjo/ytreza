import pytest

from ytreza.arch.response_object import ResponseFailure as RF
from ytreza.arch.response_object import ResponseSuccess as RS
from ytreza.arch import request_object as rqo


@pytest.fixture
def response_value():
    return {'key': ['value1', 'value2']}


@pytest.fixture
def response_type():
    return 'ResponseError'


@pytest.fixture
def response_message():
    return 'This is a response error'


def test_response_success_is_true(response_value):
    assert bool(RS(response_value)) is True


def test_response_failure_is_false(response_type, response_message):
    assert bool(RF(response_type, response_message)) is False


def test_response_success_contains_value(response_value):
    response = RS(response_value)

    assert response.value == response_value


def test_response_failure_has_type_and_message(response_type,
                                               response_message):
    response = RF(response_type, response_message)

    assert response.type == response_type
    assert response.message == response_message


def test_response_failure_has_value(response_type, response_message):
    response = RF(response_type, response_message)

    assert response.value == {'type': response_type,
                              'message': response_message}


def test_response_failure_initialization_with_exception(response_type):
    response = RF(response_type,
                  Exception('Just an error message'))

    assert bool(response) is False
    assert response.type == response_type
    assert response.message == 'Exception: Just an error message'


def test_response_failure_from_invalid_request_object():
    iro = rqo.InvalidRequestObject()
    response = RF.build_from_invalid_request_object(iro)

    assert bool(response) is False


def test_response_failure_from_invalid_request_object_with_errors():
    iro = rqo.InvalidRequestObject()
    iro.add_error('path', 'Is mandatory')
    iro.add_error('path', "can't be blank")

    response = RF.build_from_invalid_request_object(iro)

    assert bool(response) is False
    assert response.type == RF.PARAMETERS_ERROR
    assert response.message == "path: Is mandatory\npath: can't be blank"


def test_response_failure_build_resource_error():
    response = RF.build_resource_error("test message")

    assert bool(response) is False
    assert response.type == RF.RESOURCE_ERROR
    assert response.message == "test message"


def test_response_failure_build_parameters_error():
    response = RF.build_parameters_error("test message")

    assert bool(response) is False
    assert response.type == RF.PARAMETERS_ERROR
    assert response.message == "test message"


def test_response_failure_build_system_error():
    response = RF.build_system_error("test message")

    assert bool(response) is False
    assert response.type == RF.SYSTEM_ERROR
    assert response.message == "test message"


def test_response_failure_as_dict():
    response = RF.build_system_error("test message")

    assert bool(response) is False
    assert response.as_dict() == {
        'type': RF.SYSTEM_ERROR,
        'message': 'test message'
    }
