from ytreza.arch import request_object as rq
from ytreza.arch import domain_model as dm


class ModelTest(dm.DomainModel):
    value_1 = dm.Field()
    value_2 = dm.Field()


class RequestObjectTest(rq.ValidRequestObject):
    model = rq.Parameter("model", null=False)
    value_1 = rq.Validator("model.value_1", empty=False, null=False)
    value_2 = rq.Validator("model.value_2", empty=True, null=False)

    @classmethod
    def validate(cls, model, **kwargs):
        if model.value_1 == "value_1 valid":
            if model.value_2 != "value_2 valid":
                yield ("model.value_2", "invalid value")


class RequestObjectTestWithoutValidate(rq.ValidRequestObject):
    model = rq.Parameter("model", null=False)
    value_1 = rq.Validator("model.value_1", empty=False, null=False)
    value_2 = rq.Validator("model.value_2", empty=True, null=False)


class RequestObjectTestWithDefault(rq.ValidRequestObject):
    value_1 = rq.Parameter("value_1", null=False, empty=False, default=1)
    value_2 = rq.Parameter("value_2", null=False, empty=True, default=2)
    value_3 = rq.Parameter("value_3", null=True, empty=False, default=3)
    value_4 = rq.Parameter("value_4", null=True, empty=True, default=4)


def test_extract_value():
    model = ModelTest(value_1="value 1", value_2="value 2")
    d = {"model": model}

    assert rq.extract_value(d, "model") is model
    assert rq.extract_value(d, "model.value_1") == "value 1"
    assert rq.extract_value(d, "model.value_2") == "value 2"


def test_request_object_without_parameter():
    request = RequestObjectTest()

    assert request.has_errors()
    assert len(request.errors) == 1
    assert request.errors[0]['parameter'] == 'model'
    assert request.errors[0]['message'] == "Is mandatory"
    assert bool(request) is False


def test_request_object_with_null_parameter():
    model = ModelTest()
    request = RequestObjectTest(model=model)

    assert request.has_errors()
    assert len(request.errors) == 2
    assert request.errors[0]['parameter'] == 'model.value_1'
    assert request.errors[0]['message'] == "Is mandatory"
    assert request.errors[1]['parameter'] == 'model.value_2'
    assert request.errors[1]['message'] == "Is mandatory"

    assert bool(request) is False


def test_request_object_with_empty_parameter_01():
    model = ModelTest(value_1="", value_2="")
    request = RequestObjectTest(model=model)

    assert request.has_errors()
    assert len(request.errors) == 1
    assert request.errors[0]['parameter'] == 'model.value_1'
    assert request.errors[0]['message'] == "Can't be empty"

    assert bool(request) is False


def test_request_object_with_empty_parameter_02():
    model = ModelTest(value_1="value", value_2="")
    request = RequestObjectTest(model=model)

    assert not request.has_errors()
    assert bool(request) is True
    assert request.model is model
    assert request.value_1 == "value"
    assert request.value_2 == ""


def test_request_object_validate():
    model = ModelTest(value_1="value_1 valid", value_2="bas value")
    request = RequestObjectTest(model=model)

    assert request.has_errors()
    assert bool(request) is False
    assert request.errors[0]["parameter"] == "model.value_2"
    assert request.errors[0]["message"] == "invalid value"


def test_request_object_without_validate():
    model = ModelTest(value_1="value_1 valid", value_2="bas value")
    request = RequestObjectTestWithoutValidate(model=model)

    assert not request.has_errors()
    assert bool(request) is True
    assert request.model is model
    assert request.value_1 == model.value_1
    assert request.value_2 == model.value_2


def test_request_object_with_default():
    request = RequestObjectTestWithDefault()

    print(request)
    assert not request.has_errors()
    assert bool(request) is True
    assert request.value_1 == 1
    assert request.value_2 == 2
    assert request.value_3 == 3
    assert request.value_4 == 4
