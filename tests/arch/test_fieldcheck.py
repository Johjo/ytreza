import pytest
from types import SimpleNamespace
from ytreza.arch import fieldchecker as fc


@pytest.fixture
def geo_list():
    lst = [None,
           SimpleNamespace(latitude=None, longitude=None),
           SimpleNamespace(latitude=-90.01, longitude=-180.01),
           SimpleNamespace(latitude=90.01, longitude=180.01),
           SimpleNamespace(latitude=-90.00, longitude=-180.00),
           SimpleNamespace(latitude=90.00, longitude=180.00),
           SimpleNamespace(latitude=45.54, longitude=90.47)]
    return lst


def test_text_is_empty():
    assert fc.text_is_empty(None) is True
    assert fc.text_is_empty("") is True
    assert fc.text_is_empty("    ") is True
    assert fc.text_is_empty("a") is False
    assert fc.text_is_empty("   . ") is False


def test_latitude_is_valid(geo_list):
    assert fc.latitude_is_valid(geo_list[0]) is False
    assert fc.latitude_is_valid(geo_list[1]) is False
    assert fc.latitude_is_valid(geo_list[2]) is False
    assert fc.latitude_is_valid(geo_list[3]) is False
    assert fc.latitude_is_valid(geo_list[4]) is True
    assert fc.latitude_is_valid(geo_list[5]) is True
    assert fc.latitude_is_valid(geo_list[6]) is True


def test_longitude_is_valid(geo_list):
    assert fc.longitude_is_valid(geo_list[0]) is False
    assert fc.longitude_is_valid(geo_list[1]) is False
    assert fc.longitude_is_valid(geo_list[2]) is False
    assert fc.longitude_is_valid(geo_list[3]) is False
    assert fc.longitude_is_valid(geo_list[4]) is True
    assert fc.longitude_is_valid(geo_list[5]) is True
    assert fc.longitude_is_valid(geo_list[6]) is True
